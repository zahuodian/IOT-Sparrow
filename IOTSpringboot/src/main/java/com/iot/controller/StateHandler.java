package com.iot.controller;

import com.iot.entity.State;
import com.iot.repository.StateRepotory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/state")
public class StateHandler {

    @Autowired
    private StateRepotory stateRepotory;
    // 存入状态
    @PostMapping("/saveState")
    public String save(@RequestBody State state){
        System.out.println("开始接收数据！");
        State result = stateRepotory.save(state);
        if (result!=null){
            System.out.println("接收成功！");
            return "success";
        }else {
            System.out.println("接收失败！");
            return "error";
        }
    }
    // 查询状态
    @GetMapping("/findState/{number}")
    public State findById(@PathVariable("number") Integer id) {
        return stateRepotory.findById(id).get();
    }
}
