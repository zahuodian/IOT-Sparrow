package com.iot.controller;

import com.iot.entity.Device;
import com.iot.repository.DeviceRepotory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/device")
public class DeviceHandler {

    @Autowired
    private DeviceRepotory deviceRepotory;
    // 存入设备的信息
    @PostMapping("/saveDevice")
    public String save(@RequestBody Device device){
        System.out.println("开始接收数据！");
        Device result = deviceRepotory.save(device);
        if (result!=null){
            System.out.println("接收成功！");
            return "success";
        }else {
            System.out.println("接收失败！");
            return "error";
        }
    }
    // 根据ID查询设备信息
    @GetMapping("/findDevice/{id}")
    public Device findById(@PathVariable("id") Integer id) {
        return deviceRepotory.findById(id).get();
    }
    // 设备信息的分页展示
    @GetMapping("/findAll/{page}/{size}")
    public Page<Device> findAll(@PathVariable("page") Integer page, @PathVariable("size") Integer size) {
        Pageable pageable = PageRequest.of(page-1, size);
        return deviceRepotory.findAll(pageable);
    }

    @PutMapping("/update")
    public String update(@RequestBody Device device){
        Device result = deviceRepotory.save(device);
        if (result!=null){
            return "success";
        }else {
            return "error";
        }
    }

    @DeleteMapping("/deleteById/{id}")
    public void deleteById(@PathVariable("id") Integer id){
        deviceRepotory.deleteById(id);

    }

}
