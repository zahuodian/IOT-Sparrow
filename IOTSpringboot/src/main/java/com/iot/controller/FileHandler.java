package com.iot.controller;

import com.iot.entity.File;
import com.iot.repository.FileRepotory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/file")
public class FileHandler {
    @Autowired
    private FileRepotory fileRepotory;
    @GetMapping("/findAll/{page}/{size}")
    public Page<File> findAll(@PathVariable("page") Integer page, @PathVariable("size") Integer size) {
        Pageable pageable = PageRequest.of(page-1, size);
        return fileRepotory.findAll(pageable);
    }
    @PostMapping("/save")
    public String save(@RequestBody File file){
        System.out.println("-------------------\n");
        System.out.println(file);
        System.out.println("-------------------\n");
        File result = fileRepotory.save(file);
        if (result!=null){
            return "success";
        }else {
            return "error";
        }
    }
}
