package com.iot.repository;

import com.iot.entity.Device;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DeviceRepotory extends JpaRepository<Device,Integer> {

}
