package com.iot.repository;

import com.iot.entity.File;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FileRepotory extends JpaRepository<File,Integer> {

}
