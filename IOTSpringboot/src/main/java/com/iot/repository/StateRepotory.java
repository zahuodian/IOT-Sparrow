package com.iot.repository;

import com.iot.entity.State;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StateRepotory extends JpaRepository<State,Integer> {

}
