/*
SQLyog Ultimate v11.22 (64 bit)
MySQL - 5.6.50-log : Database - iot
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`iot` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `iot`;

/*Table structure for table `device` */

DROP TABLE IF EXISTS `device`;

CREATE TABLE `device` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `state` tinyint(1) DEFAULT NULL,
  `port` int(6) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `ipaddress` varchar(20) DEFAULT NULL,
  `workspace` varchar(20) DEFAULT NULL,
  `remark` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

/*Data for the table `device` */

insert  into `device`(`id`,`state`,`port`,`name`,`ipaddress`,`workspace`,`remark`) values (1,0,22,'加湿器','192.168.1.4','客厅','缓解室内干燥'),(2,1,22,'手机','192.168.167.86','卧室','随身定位'),(3,1,34,'智能电视','192.168.4.3','客厅','娱乐'),(4,1,32,'小米手环','192.168.1.6','随身','健康数据'),(5,1,11,'门铃','192.168.1.7','室外','提醒'),(6,1,24,'电脑','192.168.2.2','书房','工作');

/*Table structure for table `file` */

DROP TABLE IF EXISTS `file`;

CREATE TABLE `file` (
  `id` int(50) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `author` varchar(20) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `count` varchar(100) DEFAULT NULL,
  `path` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Data for the table `file` */

insert  into `file`(`id`,`name`,`author`,`date`,`count`,`path`) values (1,'杂货店片头示例片段.mp4','李东东',NULL,'0','C:\\Users\\Dongeast\\files\\杂货店片头示例片段.mp4'),(2,'0944a6c5d8bb2341afdc38ddcf23d730.jpg','李东东',NULL,'0','C:\\Users\\Dongeast\\files\\0944a6c5d8bb2341afdc38ddcf23d730.jpg'),(3,'back.png','李东东',NULL,'0','C:\\Users\\Dongeast\\files\\back.png');

/*Table structure for table `func` */

DROP TABLE IF EXISTS `func`;

CREATE TABLE `func` (
  `number` int(50) NOT NULL AUTO_INCREMENT,
  `device` varchar(50) DEFAULT NULL,
  `func` varchar(50) DEFAULT NULL,
  `code` varchar(100) DEFAULT NULL,
  `remark` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`number`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `func` */

insert  into `func`(`number`,`device`,`func`,`code`,`remark`) values (1,'1','1','1','1');

/*Table structure for table `state` */

DROP TABLE IF EXISTS `state`;

CREATE TABLE `state` (
  `number` int(50) NOT NULL AUTO_INCREMENT,
  `location` varchar(50) DEFAULT NULL,
  `var` varchar(50) DEFAULT NULL,
  `state` varchar(50) DEFAULT NULL,
  `time` datetime DEFAULT NULL,
  PRIMARY KEY (`number`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

/*Data for the table `state` */

insert  into `state`(`number`,`location`,`var`,`state`,`time`) values (1,'客厅','温度','28','2021-08-06 21:18:01'),(2,'客厅','湿度','56','2021-08-06 21:18:47'),(3,'客厅开关',NULL,'true',NULL),(4,'手机定位','经度','113.542246',NULL),(5,'手机定位','纬度','34.823207',NULL);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
