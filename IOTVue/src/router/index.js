import Vue from 'vue'
import VueRouter from 'vue-router'
import DeviceUpdate from '../views/DeviceUpdate.vue'
import DeviceManage from '../views/DeviceManage.vue'
import AddDevice from '../views/AddDevice'
import Execute from "../views/Execute"
import Infomation from "../views/Infomation"
import Index from "../views/Index"
import Cloud from "../views/Cloud";
import Location from "../views/Location";

Vue.use(VueRouter)

const routes = [


  {
    path: '/',
    name: "设备管理",
    component: Index,
    redirect:"/deviceManage",
    show:true,
    children:[

      {
        path: '/deviceManage',
        name: "查询设备",
        component: DeviceManage
      },
      {
        path: '/addDevice',
        name: "添加设备",
        component: AddDevice
      },
    ],
  },
  {
    path: '/Infomation',
    name: "通用功能",
    component: Index,
    show:true,
    children:[

      {
        path: '/infomation',
        name: "信息展示",
        component: Infomation
      },
      {
        path: '/execute',
        name: "设备操作",
        component: Execute
      },
    ]
  },
  {
    path: '/Home',
    name: "家庭功能",
    component: Index,
    show:true,
    children:[

      {
        path: '/cloud',
        name: "家庭云盘",
        component: Cloud
      },
      {
        path: '/location',
        name: "成员守护",
        component: Location
      },
    ]
  },
  {
    path: '/update',
    show: false,
    component: DeviceUpdate
    }


]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
