### 最新资源更新，请关注微信公众号 **“修电脑的杂货店”** 
### 官方QQ交流群： **559369389** 

![项目LOGO](https://images.gitee.com/uploads/images/2021/0823/013021_2b73607d_9620769.png "屏幕截图.png")

# 杂货店牌智能家居系统实操手册


## 手册大纲

### 第一章 前后端分离的Web项目

### 系统预览

#### 1.1前端开发

- 前端开发概述
- 技术分析
- 资料准备
- 环境搭建
- 实现基本页面搭建
- 配套教学视频链接

#### 1.2后端开发

- 后端概述
- 技术分析
- 资料准备
- 环境搭建
- 实现基本功能框架
- 配套教学视频链接

### 第二章 安卓APP开发指南

- 为什么选择开发安卓APP
- 需求分析
- 开发目标
- 搭建环境
- 实现简单的界面
- 实现功能
- 配套教学视频链接

### 第三章 基于WiFi芯片的开源硬件

- Ardiono篇（UNO、Esp8266）
- STM32篇（FreeRTOS）
- 配套教学视频链接

### 第四章 帮你监管系统的QQ机器人

- QQ机器人的优点
- Python开发框架
- 开发环境的搭建
- 实现基础功能
- 配套教学视频链接

### 第五章 系统优化技术

- 日志
- 接口的封装
- 报错的统一管理
- 事务
- 回滚

------

## 系统预览

### Web界面

#### 设备管理界面

![image-20210807152243256](https://dongeast.github.io/picture/img/image-20210807152243256.png)

#### 添加设备界面

![image-20210807152311688](https://dongeast.github.io/picture/img/image-20210807152311688.png)

#### 信息展示（待开发）

![image-20210807152344418](https://dongeast.github.io/picture/img/image-20210807152344418.png)

#### 设备操作（设备接入）

![image-20210807152418960](https://dongeast.github.io/picture/img/image-20210807152418960.png)

#### 家庭云盘

![image-20210807152516751](https://dongeast.github.io/picture/img/image-20210807152516751.png)

#### 设备定位

![image-20210807152544175](https://dongeast.github.io/picture/img/image-20210807152544175.png)

## 正文

### 第一章 前后端分离的Web项目

Web项目我们使用当下流行的开源框架搭建，采用前后端分离的技术来完成Web项目的搭建！

#### 1.1前端开发

#### 前端开发概述

前端开发店长水平有限，只能实现基本的功能，正因为本开发手册，是为了让大家能够独自完成简单的智能家居系统的搭建而编写的，所以，也会尽量的使用基础的开发能力，来让大多数人都有跟上的机会！如果有大佬或者同学愿意加入本系统的开发，那是最好不过了！欢迎大家，一起研究我们自己的开源系统~

#### 技术分析

首先，介绍一下前端开发的技术：

前端开发使用组件化开发的Vue框架，我们能够站在开源的平台，搭建属于自己的项目。对于搭建前端的界面，我们的需求就是随心所欲的搭建自己想要的界面，满足我们所想要的功能。利用现成的组件开发，无异于让我们专注于对页面的开发实现和对页面跳转逻辑的设计！让我们的时间尽可能的用于构思我们的系统，而不用在意具体的css样式是如何实现的或者html的标签的书写，以上这两者都是店长懒得学的东西，哈哈哈哈哈！但是做前端的同学也很棒，我所熟知的同学，能够按照设计师的样式，独立写出页面的设计，让我羞愧不已！

上面已经提到了组件化开发，我们接下来就仔细的说说，什么是组件化开发？就我们系统开发的需求来看，就是使用现成的UI控件：按钮、输入框、下拉框来实现我们自己的界面。组件大多具有：简洁、可复用、易修改的特点，这样开发出来的界面，简洁大方！

我们系统选用的UI组件是：Element-UI来嵌入Vue项目！到此为止，我们需要记得的知识点有：Element-UI、Vue开发框架！接下来我们就开始我们的探索之旅啦！！！

#### 资料准备

不管我们开始学习什么内容，官网都是第一位的资料库！对于一个好的开源框架，官网的资料丰不丰富，相关生态的建设完不完善、开发者是否活跃，这些都是很关键的要素！我们选择的Vue开发框架，相信只要是接触过的前端的同学都有所耳闻，实践证明Vue是一款非常优秀的开源框架，社区拥有众多活跃的开发者，在各大博客网站和技术分享网站，比如：CSDN、Github、Gitee、简书等网站拥有众多解决方案和思路！在我们遇到问题时，可以快速的查找到相关的资料，来帮助我们解决问题。相信2021年的大家更加懂得生态的重要性。

Element-Ui组件库，也是非常经典的解决方案，我们可以快速便捷的开发出简洁大方、功能齐全的页面，实现设计好的复杂逻辑！在JSP时代是想都不敢想的！

下面就是项目前端开发的资料的下载方式和相关网站：

**杂货店牌智能家居系统实操手册**：https://showdoc.dongeast.top/web/#/p/4a9603272983f1774cf87633e472aa19

![image-20210803225554433](https://dongeast.github.io/picture/img/image-20210803225554433.png)

Node.js官网：https://nodejs.org/zh-cn/

![image-20210803220133196](https://dongeast.github.io/picture/img/image-20210803220133196.png)

Vue-Cli官网：https://cli.vuejs.org/zh/

![image-20210803220324576](https://dongeast.github.io/picture/img/image-20210803220324576.png)

Element-UI官网：https://element.eleme.cn/#/zh-CN

![image-20210803220943743](https://dongeast.github.io/picture/img/image-20210803220943743.png)

CSDN：https://www.csdn.net/

![image-20210803220634223](https://dongeast.github.io/picture/img/image-20210803220634223.png)

简书：https://www.jianshu.com/

百度：https://www.baidu.com/

#### 环境搭建

1. 百度搜索“Node.js”，进入官网：https://nodejs.org/zh-cn/
   ![image-20210803221317074](https://dongeast.github.io/picture/img/image-20210803221317074.png)
2. 下载长期支持版
   ![image-20210803221456283](https://dongeast.github.io/picture/img/image-20210803221456283.png)
3. 找到下载的安装软件
   ![image-20210803221923028](https://dongeast.github.io/picture/img/image-20210803221923028.png)
4. 双击-->进行安装：

![image-20210803222026965](https://dongeast.github.io/picture/img/image-20210803222026965.png)

![image-20210803222117518](https://dongeast.github.io/picture/img/image-20210803222117518.png)

![image-20210803222224872](https://dongeast.github.io/picture/img/image-20210803222224872.png)

![image-20210803222345187](https://dongeast.github.io/picture/img/image-20210803222345187.png)

![image-20210803222423346](https://dongeast.github.io/picture/img/image-20210803222423346.png)

![image-20210803222511263](https://dongeast.github.io/picture/img/image-20210803222511263.png)

![image-20210803222549221](https://dongeast.github.io/picture/img/image-20210803222549221.png)

![image-20210803222632845](https://dongeast.github.io/picture/img/image-20210803222632845.png)

![image-20210803222720500](https://dongeast.github.io/picture/img/image-20210803222720500.png)

![image-20210803222851609](https://dongeast.github.io/picture/img/image-20210803222851609.png)

输入“node -v”，如果返回版本号，即安装成功！

![image-20210803223026733](https://dongeast.github.io/picture/img/image-20210803223026733.png)

同时会安装“npm”

![image-20210803223202508](https://dongeast.github.io/picture/img/image-20210803223202508.png)

接下来安装“cnpm”：

![image-20210803223310485](https://dongeast.github.io/picture/img/image-20210803223310485.png)

![image-20210803223355196](https://dongeast.github.io/picture/img/image-20210803223355196.png)

```
npm install -g cnpm --registry=https://registry.npm.taobao.org
```

![image-20210803223636515](https://dongeast.github.io/picture/img/image-20210803223636515.png)

到这里，我们电脑上的node.js和cnpm就全部安装完成了！

5.百度搜索“Vue-cli”，进入Vue脚手架官网：https://cli.vuejs.org/zh/

![image-20210803223953980](https://dongeast.github.io/picture/img/image-20210803223953980.png)

``` cnpm
cnpm install -g @vue/cli		“-g 全局安装”
```

![image-20210803231716472](https://dongeast.github.io/picture/img/image-20210803231716472.png)

测试一下是否安装成功：

![](https://dongeast.github.io/picture/img/vue.png)

到这里，前端开发的环境就全部搭建完成了。开发工具可以选择VSCode或者IDEA，店长使用的是VSCode，我们就以VSCode为例！

![image-20210803224801024](https://dongeast.github.io/picture/img/image-20210803224801024.png)

6.从官网下载VSCode：https://code.visualstudio.com/

![image-20210803225355601](https://dongeast.github.io/picture/img/image-20210803225355601.png)

#### 实现基本页面搭建

Windows下，在搜索框输入“cmd”，打开命令行窗口，输入“vue-ui”命令。

![image-20210803231805921](https://dongeast.github.io/picture/img/image-20210803231805921.png)

打开图形化配置界面，这是Vue3.0以上支持的一种图形化界面配置方式，图形化配置方式依赖于打开的命令行窗口，关闭上面的窗口，图形化界面也会终止。

![image-20210803232012393](https://dongeast.github.io/picture/img/image-20210803232012393.png)

![image-20210803235945066](https://dongeast.github.io/picture/img/image-20210803235945066.png)

![image-20210804000204541](https://dongeast.github.io/picture/img/image-20210804000204541.png)

![image-20210804000251611](https://dongeast.github.io/picture/img/image-20210804000251611.png)

![image-20210804000404886](https://dongeast.github.io/picture/img/image-20210804000404886.png)

![image-20210804000513350](https://dongeast.github.io/picture/img/image-20210804000513350.png)

![image-20210804000639421](https://dongeast.github.io/picture/img/image-20210804000639421.png)

![image-20210804000711419](https://dongeast.github.io/picture/img/image-20210804000711419.png)

![image-20210804000748220](https://dongeast.github.io/picture/img/image-20210804000748220.png)

接下来等待片刻，这样一个Vue项目工程就创建好了······接着我们使用VSCode打开新创建的项目

![image-20210804001114033](https://dongeast.github.io/picture/img/image-20210804001114033.png)

弹出如下对话框时，请相信自己！

![image-20210804001149474](https://dongeast.github.io/picture/img/image-20210804001149474.png)

点击项目的文件夹区域，右键选择在继承终端中打开：

![image-20210804001353335](https://dongeast.github.io/picture/img/image-20210804001353335.png)

使用“cd ..”回退到Vuetest（项目文件夹下）：

![image-20210804001535262](https://dongeast.github.io/picture/img/image-20210804001535262.png)

使用指令：“npm run serve” 运行程序：

![image-20210804001747918](https://dongeast.github.io/picture/img/image-20210804001747918.png)

”Ctrl + 鼠标左键点击“ ------ 打开网址访问 http://localhost:8080

![image-20210804001852233](https://dongeast.github.io/picture/img/image-20210804001852233.png)

到这里，我们的前端基本开发准备工作就已经做好啦！！！

#### 配套教学视频链接

https://space.bilibili.com/383001841

#### 1.2后端开发

#### 后端概述

后端开发我们只是需要用到一些基础的SpringBoot开发框架的使用方法，店长更希望大家一起来完善它的功能使它更加强大。前端通过axios方法调用后端的接口，后端实现接口，完成对数据的增删改查，在后端的开发中，秉持着SpringBoot的理念，尽可能简化对增删改查的Sql语句的书写，我们使用包装好的 JpaRepository类，进行对数据库的操作。

#### 技术分析

首先我们，介绍一下在项目后端开发过程中，我们需要用到的技术条件。在项目中，我们使用SpringBoot框架对项目进行工程化开发，我们选择导入框架封装好的数据库操作类JPA，完成对 **MySql** 数据库的增删改查：

``` java
import org.springframework.data.jpa.repository.JpaRepository;
```

创建实体类来和表，进行绑定！

后端开发需要注意一下几个点：

1. 为了项目体积变大以后，代码仍具有可读性和简洁性，我们需要按照工程化的思想来开发我们的项目。
2. 使用Javadoc生成文档注释。
3. 定义固定的返回类型，来规范方法返回的类型，便于前后端接口的统一和复用性。
4. 使用 Swagger 后可以直接通过代码生成文档，不再需要自己手动编写接口文档了。

#### 资料准备

Spring官网：https://spring.io/

![image-20210804234640846](https://dongeast.github.io/picture/img/image-20210804234640846.png)

Springboot开发文档：https://spring.io/projects/spring-boot

![image-20210804235033879](https://dongeast.github.io/picture/img/image-20210804235033879.png)

#### 环境搭建

> **jdk1.8**
>
> **maven3.6.1**
>
> **Springboot最新版**
>
> **IDEA**

#### 实现基本功能框架

##### 第一种新建项目的方法

Springboot开发文档：https://spring.io/projects/spring-boot

![image-20210804235033879](https://dongeast.github.io/picture/img/image-20210804235033879.png)

点击”QuickStart“，去生成我们的项目模板！

![image-20210804235207935](https://dongeast.github.io/picture/img/image-20210804235207935.png)

先来配置左边这一大块！！！配置完后不要结束，还有右边需要加入依赖！

![image-20210804235814736](https://dongeast.github.io/picture/img/image-20210804235814736.png)

添加右边的依赖：

![image-20210804235958830](https://dongeast.github.io/picture/img/image-20210804235958830.png)

![image-20210805000044665](https://dongeast.github.io/picture/img/image-20210805000044665.png)

这样就配置完成了，点击右下角的生成即可！

![image-20210805000220409](https://dongeast.github.io/picture/img/image-20210805000220409.png)

下载完成：

![image-20210805000256395](https://dongeast.github.io/picture/img/image-20210805000256395.png)![image-20210805000318372](https://dongeast.github.io/picture/img/image-20210805000318372.png)

Spring Boot项目现在已经下载完毕！

解压后，是一个标准的Maven项目！

![image-20210805000550179](https://dongeast.github.io/picture/img/image-20210805000550179.png)

将项目导入IDEA：

![image-20210805001300301](https://dongeast.github.io/picture/img/image-20210805001300301.png)

选择一个Maven项目导入：

![image-20210805001409796](https://dongeast.github.io/picture/img/image-20210805001409796.png)

选中路径，打开项目，基础配置可以全部默认，直接点击”Next“：

![image-20210805001637311](https://dongeast.github.io/picture/img/image-20210805001637311.png)

![image-20210805001759877](https://dongeast.github.io/picture/img/image-20210805001759877.png)

![image-20210805001824900](https://dongeast.github.io/picture/img/image-20210805001824900.png)

![image-20210805001851110](https://dongeast.github.io/picture/img/image-20210805001851110.png)

切换一下主题，项目会自动下载很多包，需要等待一段时间：

![image-20210805001922164](https://dongeast.github.io/picture/img/image-20210805001922164.png)

自动加载完毕，我们开始，运行一下看看效果：

![image-20210805002317497](https://dongeast.github.io/picture/img/image-20210805002317497.png)

![image-20210805003400109](https://dongeast.github.io/picture/img/image-20210805003400109.png)

**如果报错：”error:java 无效的源发行版11“**

![img](https://dongeast.github.io/picture/img/20200420111939376.gif)

##### 第二种新建项目的方法

直接使用IDEA新建一个Spring Boot项目。

这样我们就搭建好了，后端开发的基础项目！

#### 配套教学视频链接

##### https://space.bilibili.com/383001841

### 第二章 安卓APP开发指南

#### 为什么要开发安卓APP？

为什么要开发一个安卓APP？一个项目必须得有一个前端界面来展示，否则就算硬件做的非常完善，别人都看不出来，只有用优美简洁的界面展示复杂的操作，别人才会懂，项目有多强！

安卓开发对于简单的逻辑设计和页面控制有很好的适用性，安卓的开发过程中，可以使用控件拖拽出一个简单的界面，完全可以符合自己的审美和需求，想要更好看的界面只需要花点心思布局就好了！安卓有很多开源的项目可以使用，加上手机的便捷性，使得使用安卓作为控制端和展示端都有着独特的优势，你可以尽情的调用手机任何功能，做你想做的事：震动、NFC识别、拍照、人脸识别、WiFi、蓝牙、陀螺仪，简直就是硬件宝藏库！！！安卓开发可以选择Java或者是Python，但是Java是主流，如果不是Python特别精通还是使用Java更好一些。

当然，如果不想开发安卓APP，使用网页端，或者微信小程序、QQ小程序、支付宝小程序，都可以接入我们的系统！只是方法不同，这里先讲解安卓APP接入的方法，如果大家感兴趣的话，以后再讲讲小程序的接入。

#### 需求分析

在开发安卓软件之前我们需要理清楚，我们想用它来做些什么？首先，物联网项目中最常见的就是温湿度传感器等设备了，最基础的功能应该是展示设备的各种信息，那么问题就来了，我们怎么样保证手机和后台可以通信呢？我们选用MQTT协议来完成数据之间的传递，硬件嵌入式设备接入方式也比较简单！其次，我们希望整个系统可以知道我们在哪个地方，如果设备可以发现我们离家的距离越来越近就可帮助我们做一些事情，或者采取手机的一些传感器数据来判断我们的状态，更好地利用智能设备来为我们服务！其次，我们同学大多备受百度网盘的罪，如果我们的服务器性能还可以，那我们大可搭建自己的家庭网盘！

#### 开发目标

要求代码整洁，可复用程度高，尝试使用Spring Boot框架开发，使用一些基础的开发知识，开发前做好规划和目标。

#### 搭建环境

Android Studio最新版

#### 实现简单的界面

创建一个新的项目

#### 实现功能

1.MQTT收发功能，选用MQTT作为通信协议，MQTT协议更符合物联网的特性；
2.获取web平台上的数据和内容，用于展示整个平台设备的运行状态和设备采集的数据信息；
3.实时上传位置信息，调用安卓手机的GPS模块，获取当前位置，用于系统的智能处理；
4.实现手机文件上传和下载功能，配合着网页端在服务器上搭建属于自己的云端网盘；（受限于服务器的带宽和磁盘容量）

#### 配套教学视频链接

https://space.bilibili.com/383001841



### 第三章 基于WiFi芯片的开源硬件

#### Ardiono篇（UNO、Esp8266）

首先介绍Arduino入门的开发板经典的UNO板：

![img](https://dongeast.github.io/picture/img/r3-2.jpg)

多种类型具有WiFi功能的Esp8266芯片：

##### 重点：Esp8266芯片的分类

如果你在网上买Esp8266芯片，你就会发现有好多种类，如果你是刚入门Esp8266这款WiFi芯片，那么你在开发的过程中，可能就会被搞的，一团懵！

首先我们来看看这款芯片都有哪些样子：

Esp8266-01：

![esp826601s](https://dongeast.github.io/picture/img/esp826601s.png)

Esp8266-12：

![esp8266-12](https://dongeast.github.io/picture/img/esp8266-12.png)

Esp8266-12F：

![12f80120-275b7aabf9da929c](https://dongeast.github.io/picture/img/12f80120-275b7aabf9da929c.jpg)

Esp8266-12E（NodeMCU）:

![80120-422c3fbfafa81eb0](https://dongeast.github.io/picture/img/80120-422c3fbfafa81eb0.jpg)

NodeMCU的引脚图：

![img](https://dongeast.github.io/picture/img/HTB1zaJjavfsK1RjSszbq6AqBXXaw.jpg)

首先，我们需要有一个大概的印象，**所有的这些板核心还都是基于ESP8266(EX)来构建的**。也就是说，上面的开发板，虽然形状各有个的特色，但是芯片是一模一样的，功能完全一样！开发方法完全一样！**板子的大小不是由芯片决定的，而是有板载外设决定的！**

我们可以仔细的看，上面的各种开发板，如果把Esp-12、12F、NodeMCU的芯片金属外壳剥掉，其实全都是和Esp-01长得一个样子。其他看起来很强的样子，也就是多了一个可以抗干扰的金属外壳！

看到这里，也许大家就要问了，同一款芯片为什么要搞得这么花里胡哨的？这就是这款芯片的成功之处！一款芯片，体积小，可以使它本身可以做成非常小的尺寸，满足嵌入式开发的需要，也可以挡住一块开发板的核心板，撑起一大堆功能。另外一个原因就是，这块芯片是开源的，肯定就会有人来拿走，改成合适自己的尺寸来方便使用，大中小都有自己的流行版本！

##### 如何进行开发？

说到对芯片的开发，我们需要明白两个芯片的意义：

1. 一个叫板载芯片，也就是一块开发板中执行我们烧写程序的芯片，这块芯片很重要，相比而言，其他的板载设备可以统称为外设。烧写代码，这个动作的意义就在于把代码写进板载芯片中，而烧写的方法有很多，比如：使用TX、RX引脚；使用USB转串口芯片烧写；甚至是使用专门的烧写器；
2. 一个是转接芯片，比如常用的CH340、TTL转串口芯片，这些芯片都是为了方便对板载芯片烧写才存在的！这里我要告诉大家，向芯片里烧写程序的方法一般都是同一种方法，那就是通过板载芯片的TX、RX引脚来烧写，而使用CH340芯片（USB转串口芯片）将烧写方式升级为USB烧写，甚至开发专属的烧写器烧写，全都是对这一种烧写方式的简化！只是为了操作起来更加方便！CH340芯片的作用也只是将USB串口中的数据处理后重新传递给TX、RX引脚。

上面说到，我们对芯片进行开发，目的就是为了对芯片进行编程，让芯片完成我们指定的操作。也就是我们需要改变芯片存储的代码，如果这样理解，代码开发就可以分为两种方式。

1. 在硬件的基础上，烧写自己的代码。将自己写的代码通过编译后，烧写进芯片中，芯片运行的是我们自己的代码！
2. 在硬件的基础上烧写官方的镜像，利用TX、RX串口向芯片传入参数，芯片运行的是官方的代码，我们只是传入了参数，告诉它具体怎么做这件事。这种方式最典型的就是AT指令集。这种方式经常使芯片工作在下位机的身份，作为上位机的小弟！上位机发什么指令它就做什么，帮助上位机完成自己的工作。

说了这么多，一定要自己好好品一品其中的道理，这对我们理解硬件开发很有帮助！

##### 对Esp8266的开发方式

1. 使用烧写器的转接芯片连接电脑
2. 使用别的板子的转接芯片连接电脑
3. 使用AT指令的开发方式

##### 使用Arduino IDE开发自己的库

为什么我们需要开发自己的库函数？

我们对硬件进行开发，就难免需要对硬件进行操作，每个硬件的引脚和功能会有差异，我们就需要对不同的硬件编写不同的功能函数，如果将函数直接写在(.ino)文件中，会使我们的代码看起来特别复杂，为了提高代码的可读性和复用程度，我们应该对我们编写的代码进行封装，然后再调用。

1.首先安装开发工具 Arduino IDE （可在群文件中下载最新版）。
2.Arduino的库文件都存放在安装目录下的  **libraries**  文件夹下，我们只需要将编写完成的库文件放在文件夹下即可使用 #include<> 调用。

![libraries](https://images.gitee.com/uploads/images/2021/0823/123520_4a7e15c0_9620769.png "屏幕截图.png")

3.我们需要编写的文件有： (.cpp) 和 (.h) 、(keywords.txt 可选 )，我们以 LED 的操作为例。

![输入图片说明](https://images.gitee.com/uploads/images/2021/0823/123950_b5c11d21_9620769.png "屏幕截图.png")

```
/*****************
LED.cpp
******************/

#include"LED.h"
#include"Arduino.h"

LED::LED(byte p,bool state):pin(p)
{
   pinMode(pin,OUTPUT);
   digitalWrite(pin,state);
}

LED::~LED()
{
    disattach();
} 
   
void LED::on()
{
    digitalWrite(pin,HIGH);
}

void LED::off()
{
   digitalWrite(pin,LOW);
}

bool LED::getState()
{
    return digitalRead(pin);
}

void LED::disattach()        //引脚回收，恢复到上电状态
{
    digitalWrite(pin,LOW);
    pinMode(pin,INPUT);
}
```


```
/*******************
LED.h
*******************/

#ifndef _LED_H__
#define _LED_H__

#include"Arduino.h"  

class LED
{
     private:
          byte pin;   

     public:          
          LED(byte p , bool state=LOW );  
          ~LED();         
          byte getPin();  
          void on();      
          void off();     
          bool getState(); 
          void disattach(); 

};

#endif
```

下面是keywords.txt 的内容，其中#开头的是注释，完全可以不写。格式：word【tab】DESCRIPTION
word就是你要高亮的关键字接着1 个 tab 键 ，然后就是DESCRIPTION。
DESCRIPTION可以取的值：
KEYWORD1    高亮类名
KEYWORD2    高亮方法名
LITERAL1       高亮常量
 
注意中间使用的是 1  个  tab 键 隔开的

```
#class (KEYWORD1)
LED    KEYWORD1
 
 
#function and method (KEYWORD2)
on    KEYWORD2
off    KEYWORD2
getState    KEYWORD2
disattach    KEYWORD2
 
 
#constant (LITERAL1)
#none
```

4.将自己写好的库文件放在文件夹中直接拖进去

![输入图片说明](https://images.gitee.com/uploads/images/2021/0823/124623_5d30044b_9620769.png "屏幕截图.png")

5.我们就可以直接在我们的主文件中引用了~ 很大程度上简化了我们的代码。

```
#include <LED.h>

LED led(7);

void setup() {

}

void loop() {
  led.on();
}
```

编译结果：

![输入图片说明](https://images.gitee.com/uploads/images/2021/0823/125122_ca10f6dd_9620769.png "屏幕截图.png")


#### STM32篇（FreeRTOS）


##### 1.什么是FreeRTOS？

考虑到关注我的读者中有许多是初学者，这里简述一下FreeRTOS。简单的来说FreeRTOS就是一种实时操作系统。

**RTOS：Real Time Operating System实时操作系统。**

FreeRTOS从字面可以将它拆分为Free + RTOS，前面Free代表一种操作系统类型的名称，就像UCOS、RTX、uclinux；后面RTOS代表实时操作系统。


##### 2.为什么要学习FreeRTOS？

这个问题在我看来，主要是因为FreeRTOS相对其他RTOS有很多优势，比较火、开源免费···。从近几年嵌入式操作系统排名的数据来看，FreeRTOS的排名在嵌入式操作系统的排名中还是比较高的。

在Micrium官网（也就是UCOS的官方网站）上面也给出了一份之前RTOS的排名。其实UCOS也是相对比较火的一款实时操作系统，和FreeRTOS最大的差别在于UCOS属于商业RTOS，项目使用需要收费。

##### 3.准备工作

万事开头难，有了开头，只要不停止前行的脚步，总会达到成功的终点。相信前面关注我的朋友都知道我分享的文章是面向基础的人群，也就是说写的比较基础，相信你们都能够理解并学会。

##### 下载FreeRTOS源码及相关资料

FreeRTOS所有的源码及更多的资料都可以进入官网查找并下载。
官网地址：
http://www.freertos.org

针对初学者主要下载源码和API文档，官网目前最新的FreeRTOS版本是V10.4.4了。

官网最新版本下载地址：
http://www.freertos.org/a00104.html

![](https://raw.githubusercontent.com/dongeast/picture/master/img/60fa5de082aa4.png?token=APYNDXVORQIXAGYJUSN5KILBBAOQG)

历史版本源码下载地址：
https://sourceforge.net/projects/freertos/files/FreeRTOS

![](https://raw.githubusercontent.com/dongeast/picture/master/img/60fa5e0c04c6a.png?token=APYNDXUDMKDVLSITFXPFRKTBBAOQ4)

参考手册下载地址：
http://www.freertos.org/Documentation/RTOS_book.html

![](https://raw.githubusercontent.com/dongeast/picture/master/img/60fa5e4d3c9bd.png?token=APYNDXU4LTYWHBTIWFWVKALBBAOR2)

##### 开发工具下载安装

我接下来讲述的FreeRTOS例程将结合MDK-ARM或者EWARM集成开发工具，也将会提供MDK-ARM或者EWARM的源代码工程。所以需要大家安装并学会使用这两种开发工具。

如果有朋友对这两种开发工具不是很熟悉，可以参看我关于MDK-ARM和EWARM的系列教程。

##### 参考文章：

1.MDK-ARM介绍、下载、安装与注册（超链接）

2.MDK-ARM_新建软件工程详细过程（超链接）

3.EWARM介绍、下载、安装与注册（超链接）

4.EWARM_新建软件工程详细过程（超链接）

##### 下载STM32标准外设库

为什么要下载STM32标准外设库呢？

直接的说：我们接下来讲述的内容是基于STM32标准外设库来展开详细讲解。

在上面下载FreeRTOS源码中，其实是包含了STM32的标准外设库，但那是很老的库。我们搞技术的人一般都有一种心理，都喜欢追求新的技术（只要不影响某些功能），特别是学习者更应该向最新的技术看齐。

https://www.st.com/en/embedded-software/stm32-standard-peripheral-libraries.html#products

![](https://raw.githubusercontent.com/dongeast/picture/master/img/60fa5f844d169.png?token=APYNDXQVLNNRFN4355QMLNTBBAOTG)

当然，这一小节主要是看硬件平台，如果你使用的其他平台的处理器，可以略过，我这里只是向初学者提供一种便利而已。学习FreeRTOS与底层硬件平台的关系不是很大，只是在移植的时候需要用上，后面学习API具体的功能时，基本不会关心硬件平台。

##### 移植几个步骤

源代码里面的内容有很少变动，本篇文章不具体讲述，只讲述移植的几个步骤，最终让代码工程可以运行在板子上。只需要如下简单几步即可：

##### 1.提取源码添加到工程
上一篇文章讲述了下载FreeRTOS源码中各个目录及文件的意思，也大概讲述了一下需要提取源码。
主要提取：Source目录 + FreeRTOSConfig.h（里面有部分内容未使用，为了不让大家感觉文件比较多，或者杂，我删除了）

![](https://raw.githubusercontent.com/dongeast/picture/master/img/20210803003432.png?token=APYNDXUXXEJZBCFZSM24A7TBBAPNM)

将源码中“Source”目录下的文件拷贝到自己的工程目录下

将这些源码添加到自己的工程（之前讲述并建立好的STM32工程），具体提取的源码和添加到工程中的效果，请下载代码查看。


##### 2.添加路径

我们添加源码到工程，有部分头文件include需要添加到工程。因此，我们需要添加相应路径。

.c源代码添加路径的方法见文章：

1.MDK-ARM_新建软件工程详细过程（超链接）
2.EWARM_新建软件工程详细过程（超链接）

这里主要想提醒的是：在IAR工程中有个汇编文件portasm.s包含了<FreeRTOSConfig.h>，需要我们添加<FreeRTOSConfig.h>所在文件下的路径。

**注意：是在EWARM的Project -> Options -> Assembler -> Preprocessor中添加。**

##### 3.创建自己任务和添加相应代码

我们创建四个任务，还是使用比较经典的LED灯任务。
```
void AppTaskCreate(void)

{
  xTaskCreate(vAppTask1, "Task1", TASK1_STACK_SIZE, NULL, TASK1_PRIORITY, NULL);
  xTaskCreate(vAppTask2, "Task2", TASK2_STACK_SIZE, NULL, TASK2_PRIORITY, NULL);
  xTaskCreate(vAppTask3, "Task3", TASK3_STACK_SIZE, NULL, TASK3_PRIORITY, NULL);
  xTaskCreate(vAppTask4, "Task4", TASK4_STACK_SIZE, NULL, TASK4_PRIORITY, NULL);
}

void vAppTask1(void *pvParameters)
{
  for(;;)
  {
    LED1_TOGGLE;
    vTaskDelay(50);
  }
}
```

我新建了app_task.c和app_task.h文件添加任务的源代码。

##### 4.修改FreeRTOSConfig.h配置文件

这个文件是必须配置的，而且根据实际情况而定。我们系统的裁剪也与该文件有关，类似于UCOS中的os_cfg.h文件。



比如：主频、系统滴答、系统堆栈大小等。

```
#define configCPU_CLOCK_HZ        ((unsigned long)72000000)
#define configTICK_RATE_HZ        ((TickType_t)100)
#define configTOTAL_HEAP_SIZE     ((size_t)(4 * 1024))
```

至此，基本的步骤算是完成了。

#### 配套教学视频链接

https://space.bilibili.com/383001841



### 第四章 帮你监管系统的QQ机器人

#### QQ机器人的优点

QQ和微信是我们生活中最经常接触到的两个实时通讯类软件，可以说普及程度已经达到了装机必备的程度，懂的都懂！如何对这两个App开发？将自己的代码融入到这两款优秀的App中，是一项多么有成就感的工作！江湖传言：小孩子都玩QQ、家长才玩微信！所以尚在年幼的我，也选择用自己最常用的QQ接入我们的项目。微信也有相应的开源自动化框架，有兴趣大家可以自行搭建哦~（机器人可千万不要用于什么不好的地方！后果很严重！）

#### Python开发框架

对于QQ机器人的开发，我们选择使用Github上一款开源框架”mirai“。它支持Java、Python等多种编程语言，为了方便后续对机器人的功能做一些拓展，所以我们选择使用Python开发。（有兴趣的小伙伴可以在官网找到相关文档使用自己喜欢的语言哦！）

我们先来了解一下它，mirai 是一个在全平台下运行，提供 QQ Android 协议支持的高效率机器人库。

项目地址：https://github.com/mamoe/mirai

![image-20210806004119759](https://dongeast.github.io/picture/img/image-20210806004119759.png)

用户手册：https://github.com/mamoe/mirai/blob/dev/docs/UserManual.md

开发文档：https://github.com/mamoe/mirai/blob/dev/docs/README.md

环境搭建流程参考网页：https://github.com/mamoe/mirai/blob/dev/docs/UserManual.md

#### 开发环境的搭建

首先，我们要为机器人找到一个QQ号（建议做好被封的准备！）

<img src="https://dongeast.github.io/picture/img/image-20210806005027605.png" alt="image-20210806005027605" style="zoom: 67%;" />

经过店长的一顿操作，QQ小号成就达成，接下来我们就按照官方文档一步一步来就好了！

一、安装Mirai的控制台：启动Mirai，使用 Mirai，一般人要启动的是 Mirai 控制台（即 Mirai Console），它可以加载插件。

1. 访问 [iTXTech/mcl-installer](https://github.com/iTXTech/mcl-installer/releases)；
2. 下载适合你的系统的可执行文件；
3. 在一个新文件夹存放这个文件，运行它；
4. 通常可以一路回车使用默认设置完成安装，安装完成后程序自动退出；
5. 运行 `mcl.cmd` 启动，成功后会看到绿色的 `mirai-console started successfully`。

![image-20210806010414177](https://dongeast.github.io/picture/img/image-20210806010414177.png)

新建一个新的文件夹来存放QQ机器人项目的文件，在这个文件夹下运行这个安装文件，会自动下载文件（大约120M）到本地。由于项目是在Github上，我们需要**多安装几遍**确保安装完整，安装不完整会导致运行各种报错！

这个下载过程可能有点慢······

![image-20210806012709455](https://dongeast.github.io/picture/img/image-20210806012709455.png)

下载完成后：

![image-20210806011101078](https://dongeast.github.io/picture/img/image-20210806011101078.png)

| 文件夹名称 | 用途                                  |
| ---------- | ------------------------------------ |
| `scripts`  | 存放启动器的脚本，一般不需要在意他们   |
| `plugins`  | 存放插件                             |
| `data`     | 存放插件的数据，一般不需要在意它们     |
| `config`   | 存放插件的配置，可以打开并修改配置     |
| `logs`     | 存放运行时的日志，日志默认保留 7 天    |

如果安装不成功会导致，运行  mcl.cmd  闪退失败！（店长会放好配置好的环境~，如果同学们想自己搭建环境可以自己按照官方文档里一步一步来，也可以在交流群里问！）

PS：也可以在这里：https://github.com/iTXTech/mirai-console-loader/releases 直接下载MCL运行环境，下载完成解压即可，无需安装。

![image-20210806013604065](https://dongeast.github.io/picture/img/image-20210806013604065.png)

![image-20210806013723163](https://dongeast.github.io/picture/img/image-20210806013723163.png)

当我安装完MCL的时候，却发生了报错：

![image-20210806013840325](https://dongeast.github.io/picture/img/image-20210806013840325.png)

错误：发生JNI错误，请检查安装并重试
线程“main”java.lang.UnsupportedClassVersionError:org/itxtech/mcl/Loader中的异常已由较新版本的java运行时（类文件版本55.0）编译，此版本的java运行时仅识别高达52.0的类文件版本

是Java运行环境不对！



#### 实现基础功能

#### 配套教学视频链接

### 第五章 系统优化技术

#### 日志

#### 接口的封装

#### 报错的统一管理

#### 事务

#### 回滚